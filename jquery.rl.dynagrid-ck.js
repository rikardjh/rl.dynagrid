/**
*	What is it?
*		A jQuery extension that takes a container and makes a responsive grid of it's child elements
*
*	Who made it?
*		Copyright 2013 Rikard Lindström rikardjh@gmail.com
*
*	Licence?
*		Free to use, free to modify. Just keep the comments above
*		MIT Licence if you need one
*
*	Found a bug?
*		Fix it! or e-mail rikardjh@gmail.com
*/(function(e){e.fn.extend({dynagrid:function(t){var n={margin:10,minWidth:120,maxWidth:140,minHeight:100,maxHeight:140,cssPrefix:"dynagrid",animationSpeed:100,guides:!0,wrapperId:this.attr("id")||"dynagrid-wrapper"};t=e.extend(n,t);return this.each(function(){var n=t,r=e(this);r.attr("id",n.wrapperId);r.updateAfterResize=function(){var t=e("#"+n.wrapperId).children().size(),r=e(this).parent().innerWidth(),i=Math.min(Math.floor(r/n.minWidth),t);(i-1)*n.margin>r&&i--;var s=r-(i-1)*n.margin,u=s/i;u=u/r*100;var a=0,f=0;this.children().each(function(t,r){var s=e(this),l=t%i===0,c=t%i===i-1;s.css({width:u+"%",height:n.minHeight,"margin-right":c?0:n.margin,"margin-bottom":n.margin});var h=s.attr("data-row")||0,p=s.attr("data-coll")||0;s.removeClass(n.cssPrefix+"_"+"row_"+h);s.removeClass(n.cssPrefix+"_"+"coll_"+p);s.addClass(n.cssPrefix+"_"+"row_"+a);s.addClass(n.cssPrefix+"_"+"coll_"+f);s.attr("data-row",a);s.attr("data-coll",f);if(c){f=0;a++}else f++})};r.updateAfterResize();e(window).bind("resize",function(){this.updateAfterResize()}.bind(r));r.children().each(function(t){var r=e(this);r.addClass(n.cssPrefix+"_"+"cell");n.guides&&r.css("background-color","rgba("+t*17%255+", 123, "+Math.abs(30-t*17)%255+", 1)");r.css("width",n.minWidth);r.css("height",n.minHeight);r.css("float","left");r.css("display","inline")});r.css("background","1px solid red");e(this).append("<div style='clear:both'></div>")})}})})(jQuery);