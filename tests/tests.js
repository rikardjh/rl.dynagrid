function setup(){
	$("#test-wrapper").dynagrid({
		guides : true,
		cssPrefix : "dt"
	});
};

function teardown(){

};

test( "hello test", function() {
  ok( 1 == "1", "Passed!" );
});

test( "append class", function(){
	setup();
	$("#test-wrapper").children().each(function(index, elem){
		ok( $(this).hasClass("dt_cell"), "Passed!" );
	});

});