/**
*	What is it?
*		A jQuery extension that takes a container and makes a responsive grid of it's child elements
*
*	Who made it?
*		Copyright 2013 Rikard Lindström rikardjh@gmail.com
*
*	Licence?
*		Free to use, free to modify. Just keep the comments above
*		MIT Licence if you need one
*
*	Found a bug?
*		Fix it! or e-mail rikardjh@gmail.com
*/
(function($){
	$.fn.extend({
		dynagrid : function(options){
			var defaults = {
				margin: 10,
				minWidth : 120,
				maxWidth : 140,
				minHeight : 100,
				maxHeight : 140,
				cssPrefix : 'dynagrid',
				animationSpeed : 100,
				guides : true,
				wrapperId : ( this.attr('id') || 'dynagrid-wrapper' )
			};

			options = $.extend(defaults, options);
			
			return this.each(function(){
				var o = options,
					obj = $(this);

				obj.attr('id', o.wrapperId);

				obj.updateAfterResize = function(){
					var numElements = $('#'+o.wrapperId).children().size(),
						parentWidth = $(this).parent().innerWidth(),
						numColls = Math.min( Math.floor( parentWidth / o.minWidth), numElements);
					//make sure there's room for margin
					if((numColls - 1) * o.margin > parentWidth)
						numColls--;

					var space = parentWidth - ((numColls - 1) * o.margin);
					var spacePerc = space / numColls;
						spacePerc = (spacePerc / parentWidth) * 100;

					var rowCount = 0;
					var collCount = 0;
					this.children().each(function(index, obj){
						var child = $(this),
							isLeftMost = index % numColls === 0,
							isRightMost = index % numColls === numColls - 1;

						child.css({
							"width" : spacePerc + "%",
							"height" : o.minHeight,
							"margin-right" : isRightMost ? 0 : o.margin,
							"margin-bottom" : o.margin
						});

						var row = child.attr('data-row') || 0;
						var coll = child.attr('data-coll') || 0;

						child.removeClass(o.cssPrefix+'_'+'row_'+row);
						child.removeClass(o.cssPrefix+'_'+'coll_'+coll);
								
						
						child.addClass(o.cssPrefix+'_'+'row_'+rowCount);
						child.addClass(o.cssPrefix+'_'+'coll_'+collCount);
							
						child.attr('data-row', rowCount);
						child.attr('data-coll', collCount);

						if(isRightMost){
							collCount = 0;
							rowCount ++;
						} else {
							collCount ++;
						}
					});
				};
				obj.updateAfterResize();
				$(window).bind("resize", function(){ 
					this.updateAfterResize(); 
				}.bind(obj));

				obj.children().each(function(index){
					var child = $(this);
					child.addClass(o.cssPrefix + '_' + 'cell');
					if(o.guides)
						child.css("background-color", "rgba("+((index*17) % 255)+", 123, "+( Math.abs(30-index*17) % 255)+", 1)");

					child.css('width', o.minWidth);
					child.css('height', o.minHeight);
					child.css('float', "left");
					child.css('display', "inline");
				});
				obj.css("background", "1px solid red");
				$(this).append("<div style='clear:both'></div>");
			
			});
		}

	});
}(jQuery));